
## Developing Services on Kumori PaaS

#### An in-depth course <!-- .element: class="subtitle" -->
 José Bernabéu 
 
  <josep@kumori.systems>
<!--
@fa[refresh fa-spin fa-3x fa-fw ku-main]()
@fa[envelope-o fa-spin fa-fw ku-dark]()
-->

---

## Kumori's service model

_Architectural approach to organize and describe all the elements of a service as well as their relationship_

+++

### Components and Microservices
<br>
-  @fa[](**Component**) <!--.element: class="fa-li"-->

   Stack of software that can be executed/stopped/reconfigured according to some conventions about its lifetime
-  @fa[heart fa-li](Microservice) Macro problem

   A component that has been launched as the first part of its lifetime
   - Another thing here
   - More items

<!-- .element: class="fa-ul" -->
<br>
<span class="fa-byline">With just one line of custom CSS.</span>
+++

-  @fa[anchor](n)
-  @fa[anchor](Component:)
-  @fa[anchor](Microservice:)



